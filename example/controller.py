import os
import sys

MODULE_PATH = os.path.join(
    os.path.dirname(__file__),
    ".."
)

sys.path.append(MODULE_PATH)

from fuse import loadPackage

BASEDIR = os.path.dirname(__file__) 
HELLO = os.path.join(
            BASEDIR,
            "hello"
        )
HELLO_NODE = os.path.join(
            BASEDIR,
            "hello-node"
        )

def main() -> None:
    hello = loadPackage(os.path.join(HELLO, "app.json"))
    
    hello.log = os.path.join(HELLO, "log.txt")
    
    print(hello("World"))
    
    helloNode = loadPackage(os.path.join(
            HELLO_NODE,
            "app.json"
        ))
    
    helloNode.log = os.path.join(HELLO_NODE, "log.txt")
    
    print(helloNode("World"))

if __name__ == "__main__":
    main()