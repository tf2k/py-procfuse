import os
import sys

MODULE_PATH = os.path.join(
    os.path.dirname(__file__),
    "../.."
)

sys.path.append(MODULE_PATH)

import fuse

@fuse.entry
def main(name:str) -> str:
    return f"Hello, {name}!"