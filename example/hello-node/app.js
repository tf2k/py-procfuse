const fuse = require("../../langs/node");

/**
 * @param { string } name 
 */
const hello = (name) => {
    return `Hello, ${name} from Node!`;
}

fuse(hello);