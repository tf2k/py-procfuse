# PyProcFuse
A simple module that help you call a process like using a normal function.

## How to use
Install using pip:
```sh
pip install procfuse
```

Check the files under folder `example` to find out how to use the module.