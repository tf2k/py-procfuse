// @ts-check

const { Console } = require("console");
const { createWriteStream } = require("fs");
const { devNull } = require("os");
const process = require("process");

/**
 * @param { (...args:any[]) => any | Promise<any> } app
 * @returns { Promise<void> } 
 */
module.exports = async function entry(app) {
    const realConsole = global.console;
    let logger;
    try {
        const args = await new Promise((resolve, reject) => {
            const buffers = [];

            process.stdin.on("data", (part) => {
                buffers.push(part);
            });

            process.stdin.on("close", (hadError) => {
                if (hadError) return reject(new Error("error when stdin close."));
                resolve(JSON.parse(Buffer.concat(buffers).toString("utf-8")));
            });

            process.stdin.on("error", (error) => {
                reject(error);
            });
        });

        const logPath = args.log || devNull;
        logger = createWriteStream(logPath);
        global.console = new Console(logger, process.stderr, false);

        const call = app.call(null, ...args.args);

        let result;
        if (call instanceof Promise) result = await call;
        else result = call;

        process.stdout.write(JSON.stringify(result));
    } finally {
        global.console = realConsole;
        await /** @type {Promise<void>} */(new Promise((resolve, reject) => {
            logger.close((error) => {
                if (error) return reject(error);
                resolve();
            });
        }))
    }
}