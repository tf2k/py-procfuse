
from setuptools import setup
setup(
    name = "procfuse",
    version = "0.0.1",
    description = "Proc Fuse Module.",
    packages = [
        "fuse"
    ],
    author="TunaFish2K",
    author_email="tunafish2k@163.com",
    license="Apache-2.0"
)